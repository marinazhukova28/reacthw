import React from 'react';
import styles from './Modal.module.scss';

class Modal extends React.PureComponent{
    render(){
        const {closeModal,title, actions,text } = this.props
        return(
            <div className={styles.background} onClick= {closeModal}>
                <div className = {styles.modal}>
                    <div className={styles.header}>
                        <h2>{title}</h2>
                        <div className = {styles.modalClose} onClick= {closeModal}>
                            ×
                            </div>
                    </div>        
                    <div className={styles.content}>
                    {text}
                    </div>
                    <div className={styles.buttons}>{actions}</div>
                </div>
            </div>
        )
    }
}
Modal.propTypes = {};
Modal.defaultProps ={}
export default Modal;
