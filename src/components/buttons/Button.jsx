import React,{Component} from "react"
import styles from "./Button.module.scss"

class Button extends React.Component{
    render (){
            const {
                openModal,
                backgroundColor,
                text
                
                // onClick 
            }=this.props

        return(
            <button className={styles.btn}
            style = {{backgroundColor}}
            onClick= {openModal}>
                {text}
            </button>
        )
    }
}

export default Button