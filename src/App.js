import React,{Component} from "react"
import styles from './App.module.scss';
import Button from './components/buttons/Button';
import Modal from './components/modals/Modal'
class App extends Component{
 state ={
  isOpenFirst: false,
  isOpenSecond: false
 }

 openModal1 = () => {
   
   this.setState({
    isOpenFirst: true,
    isOpenSecond: false
   })
  
 }

 openModal2 = () => {
   
  this.setState({
   isOpenFirst: false,
   isOpenSecond: true
  })
 
}

 closeModal = () => {
  this.setState({
    isOpenFirst: false,
    isOpenSecond: false

  })
}
  render(){
 
return (
  <>
  <div className={styles.App}>
    
  <Button
  backgroundColor = 'green'
  text = {'open first modal'}
  openModal = {this.openModal1}
  />

<Button
  backgroundColor = 'red'
  text = {'open second modal'}
  openModal = {this.openModal2}   
 
  />
  
 
{this.state.isOpenFirst && <Modal 
  title={'Your task has been added!'}
  text={"Check your email for a confirmation. Dont forget to set remineder using our mobile app!"}
  closeModal={this.closeModal}
  actions = {<>
  <button onClick={this.closeModal}>Ok</button>
  <button onClick={this.closeModal}>Cancel</button></>}
  /> }

{this.state.isOpenSecond && <Modal 
  title={'Do you want to delete this file?'}
  text={"Onse you delete this file, it won't be possible to undo this action.    Are you sure you want to delete it ?"}
  closeModal={this.closeModal}
  actions = {<>
  <button onClick={this.closeModal}>Continue</button>
  <button onClick={this.closeModal}>Close</button></>}
  /> }

  </div>
  </>
);
  }
}

export default App;
